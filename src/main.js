import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import Hotel from './Hotel.vue'
import hotelSelected from './Hotel-selected.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

Vue.use(BootstrapVue)

if (document.getElementById('hotel')){
  new Vue({
    el: '#hotel',
    render: h => h(Hotel)
  });
}

if (document.getElementById('hotel-selected')){
  new Vue({
    el: '#hotel-selected',
    render: h => h(hotelSelected)
  });
}


